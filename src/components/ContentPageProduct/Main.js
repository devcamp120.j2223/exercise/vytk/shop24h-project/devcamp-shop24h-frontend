import { Grid } from "@mui/material";
import { useParams } from "react-router-dom";
import { Button, Col, Container, Row } from "reactstrap";
import { Carousel } from "react-bootstrap";
import { useEffect, useState } from "react";
import "font-awesome/css/font-awesome.min.css";
import "./Main.css";
export default function ProductDetails() {
  const { paramId } = useParams();
  const [image, setImage] = useState([]);
  const [product, setProduct] = useState("");
  const [type, setType] = useState("");
  const [count, setCount] = useState(0);
  const [total, setTotal] = useState(0);
  
  const countProduct = JSON.parse(localStorage.getItem(`${paramId}`));
  const clickPlusHandler = () => {
    console.log("up count!");
    setCount(Number(count) + 1);
  };
  const clickMinusHandler = () => {
    console.log("down count!");
    setCount(Number(count) - 1);
  };
  const callAPI = async (url, paramOptions = {}) => {
    const res = await fetch(url, paramOptions);
    const data = await res.json();
    return data;
  };
  useEffect(() => {
    callAPI("http://localhost:8000/products/" + paramId)
      .then((data) => {
        console.log(data.data);
        setImage(data.data.imageUrl);
        setProduct(data.data);
        callAPI("http://localhost:8000/productTypes/" + data.data.type).then(
          (res) => {
            console.log(res);
            setType(res.data.name);
          }
        );
      })
      .catch((err) => {
        console.error(err.message);
      });
    if (countProduct === null||countProduct ==0) {
      console.log("countProduct: null||0");
    } else {
       new Promise((resolve,reject)=>{
        setTimeout(function() {
          var didSucceed = setCount(countProduct);
          didSucceed ? resolve(setTotal(count)) : reject('Error');
        }, 2000);
      })
    }
  }, []);
  useEffect(() => {
    //info.count=count;
    setTotal(count * product.buyPrice);
  }, [count]);
  return (
    <Container>
      <Grid container>
        <Grid
          item
          id="product"
          className="section-title"
          style={{
            marginTop: "100px",
            borderLeft: "10px solid pink",
            paddingTop: "20px",
            paddingLeft: "20px",
          }}
        >
          <h2>Product Details</h2>
          <Grid item className="breadcrumbs">
            <ol>
              <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="/products">All Product</a>
              </li>
              <li>Product Details</li>
            </ol>
          </Grid>
        </Grid>
      </Grid>
      <Grid container mt={5}>
        <Grid item md={7}>
          <Carousel>
            {image.map((item, index) => {
              return (
                <Carousel.Item key={index}>
                  <img
                    className="d-block w-100"
                    src={item}
                    alt="Product details slide"
                  />
                </Carousel.Item>
              );
            })}
          </Carousel>
        </Grid>
        <Grid item md={5}>
          <h3 style={{ textAlign: "center", color: "green" }}>
            <b>Project Infomation</b>
          </h3>
          {
            <div style={{ marginLeft: "50px", marginTop: "50px" }}>
              <h5 style={{ color: "green" }}>
                <b>Chi tiết sản phẩm: </b>
              </h5>
              <p>{product.name}</p>
              <p>
                <b>Loại: </b> {type}{" "}
              </p>
              <p>
                <b>Giá: </b> {product.buyPrice}đ - {product.promotionPrice}đ{" "}
              </p>
              <p>
                <b>Thông số kĩ thuật / Tính năng:</b>{" "}
              </p>
              <p>{product.description} </p>
            </div>
          }
          <Row style={{ alignItems: "center", marginTop: "50px",color:'green' }}>
            <Col style={{ textAlign: "right" }}>
              {count === 0||count == 0 ? null : (
                <i
                  type="button"
                  className="fa-solid fa-circle-minus fa-xl"
                  onClick={clickMinusHandler}
                ></i>
              )}
              <span style={{ marginLeft: "20px", marginRight: "20px" }}>
                {count}
              </span>
              <i
                type="button"
                className="fa-solid fa-circle-plus fa-xl"
                onClick={clickPlusHandler}
              ></i>
            </Col>
            <Col>
              <Button
              color="success"
                onClick={() => {
                  console.log("click");
                  //console.log("InfoProduct: "+info);
                  if(count==0){
                    localStorage.removeItem(`${paramId}`);
                  }else{
                    localStorage.setItem(`${paramId}`, JSON.stringify(count));
                  }
                }}
              >
                ADD TO CART
              </Button>
            </Col>
            <Col style={{ padding: "0" }}>
              {(count === 0 ||
              countProduct === null ||
              countProduct == 0) ? null : (
                <p style={{ marginBottom: "0" }}>
                  <b style={{ marginRight: "10px" }}>Total:</b>
                  {total.toLocaleString("en")}đ
                </p>
              )}
            </Col>
          </Row>
        </Grid>
      </Grid>
    </Container>
  );
}
